package brt.dawid.evote.controller;

import brt.dawid.evote.DTO.AnswerDTO;
import brt.dawid.evote.DTO.QuestionDTO;
import brt.dawid.evote.DTO.SurveyDTO;
import brt.dawid.evote.model.Answer;
import brt.dawid.evote.model.Question;
import brt.dawid.evote.model.Survey;
import brt.dawid.evote.model.User;
import brt.dawid.evote.repository.AnswerRepository;
import brt.dawid.evote.repository.QuestionRepository;
import brt.dawid.evote.repository.SurveyRepository;
import brt.dawid.evote.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@RestController
@AllArgsConstructor
@RequestMapping("/survey")
public class SurveyController {

    private SurveyRepository surveyRepository;
    private QuestionRepository questionRepository;
    private AnswerRepository answerRepository;
    private UserRepository userRepository;

    @GetMapping("/all")
    public Object getAllVisibleSurveys() {
        return surveyRepository.findVisibleSurveys();
    }

    @ResponseBody
    @GetMapping("/id:{id}")
    public Object getSurveyFromId(@PathVariable("id") String id) {
        return getSurveyFromId(id, null);
    }

    @ResponseBody
    @PostMapping("/id:{id}")
    public Object getSurveyFromId(@PathVariable("id") String id, @RequestParam("password") String password) {
        Survey survey = surveyRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("There is no survey with id: " + id));
        return getSurvey(survey, password);
    }

    @ResponseBody
    @GetMapping("/name:{name}")
    public Object getSurveyFromName(@PathVariable("name") String name) {
        return getSurveyFromName(name, null);
    }

    @ResponseBody
    @PostMapping("/name:{name}")
    public Object getSurveyFromName(@PathVariable("name") String name, @RequestParam("password") String password) {
        Survey survey = surveyRepository.findByName(name)
                .orElseThrow(() -> new NoSuchElementException("There is no survey with name: " + name));
        return getSurvey(survey, password);
    }

    private SurveyDTO getSurvey(Survey survey, String password) {
        if (survey.getPasswordRequired() != null && survey.getPasswordRequired()) {
            if (password == null) {
                throw new IllegalArgumentException("Password is required");
            } else if (!survey.getPassword().equals(password)) {//TODO: hash password
                throw new IllegalArgumentException("Wrong password");
            }
        }
        List<Question> questions = questionRepository.getAllQuestionsFromSurvey(survey.getId());
        List<QuestionDTO> questionsAndAnswers = new ArrayList<>();
        for (Question question : questions) {
            questionsAndAnswers.add(
                    new QuestionDTO(
                            question.getId(), question.getQuestion(), question.getQuestionNumber(),
                            answerRepository.findAllAnswersToQuestion(question.getId())
                                    .stream()
                                    .map(answer -> new AnswerDTO(answer.getId(), answer.getAnswer(), answer.getVotes()))
                                    .collect(Collectors.toList())));
        }
        return new SurveyDTO(survey.getName(), survey.getNote(), null, survey.getVisible(), questionsAndAnswers);
    }

    @PutMapping
    public String addSurvey(@RequestBody SurveyDTO survey) {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final String email = (String) authentication.getPrincipal();
        final User user = userRepository.findByEmail(email).orElseThrow(() -> new IllegalStateException("No such user"));
        Survey newSurvey = Survey.builder()
                .user(user)
                .name(survey.getName())
                .note(survey.getNote())
                .passwordRequired(survey.getPassword() != null && !survey.getPassword().isEmpty())
                .password(survey.getPassword())
                .visible(survey.getVisible())
                .build();

        surveyRepository.save(newSurvey);
        for (QuestionDTO question : survey.getQuestions()) {
            Question newQuestion = Question.builder()
                    .survey(newSurvey)
                    .question(question.getQuestion())
                    .questionNumber(question.getQuestionNumber())
                    .build();
            questionRepository.save(newQuestion);
            for (AnswerDTO answer : question.getAnswers()) {
                Answer newAnswer = Answer.builder()
                        .question(newQuestion)
                        .answer(answer.getAnswer())
                        .votes(0)
                        .build();
                answerRepository.save(newAnswer);
            }
        }
        return "Added new survey with id: " + newSurvey.getId();
    }

    @GetMapping("/user")
    public Object getCurrentUserSurveys() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final String email = (String) authentication.getPrincipal();
        return getUserSurveys(email);
    }

    @GetMapping("/user/{email}")
    @PreAuthorize("hasRole('ADMIN')")
    public Object getUserSurveys(@PathVariable("email") String email) {
        return surveyRepository.findSurveysByUser(email);
    }

    @Transactional
    @DeleteMapping("/id:{id}")
    public Object removeSurvey(@PathVariable String id) {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final String email = (String) authentication.getPrincipal();
        final Survey survey = surveyRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("No such Survey"));
        if (userRepository.findRolesByEmail(email).contains("ADMIN") || survey.getUser().getEmail().equals(email)) {
            for (Question question : questionRepository.getAllQuestionsFromSurvey(survey.getId())) {
                answerRepository.findAllAnswersToQuestion(question.getId())
                        .forEach(answer -> answerRepository.delete(answer));
                questionRepository.delete(question);
            }
            surveyRepository.delete(survey);
            return "Survey deleted";
        }
        throw new IllegalArgumentException("You cannot delete this survey");
    }
}

package brt.dawid.evote.controller;

import brt.dawid.evote.DTO.SimpleUserDTO;
import brt.dawid.evote.DTO.UserDTO;
import brt.dawid.evote.model.Role;
import brt.dawid.evote.model.User;
import brt.dawid.evote.model.UserRole;
import brt.dawid.evote.repository.RoleRepository;
import brt.dawid.evote.repository.UserRepository;
import brt.dawid.evote.repository.UserRoleRepository;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Set;

@RestController
@AllArgsConstructor
@RequestMapping
public class UserController {

    private static final Logger LOGGER = LogManager.getLogger();

    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private UserRoleRepository userRoleRepository;

    @GetMapping("/user")
    public Object currentUser() {
        LOGGER.info("Getting information about logged user");
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final String email = (String) authentication.getPrincipal();
        final User authenticatedUser =
                userRepository.findByEmail(email).orElseThrow(() -> new IllegalStateException("No such user"));
        return SimpleUserDTO.of(authenticatedUser.getEmail(), authenticatedUser.getFirstname(),
                authenticatedUser.getSurname(), authenticatedUser.getPoints());
    }

    @PutMapping("/signon")
    public String createNewUser(@RequestBody UserDTO user) {
        LOGGER.info("Creating new user");
        if (user.getPassword() == null || user.getPassword().isEmpty()
                || !user.getPassword().equals(user.getConfirmPassword())) {
            throw new IllegalArgumentException("Not equal passwords");
        }
        Set<Role> roles = new HashSet<>();
        user.getRoles()
                .forEach(userRole -> roles.add(roleRepository.findByName(userRole)
                        .orElseThrow(() -> new NoSuchElementException("There is no role " + userRole))));

        User newUser = User.builder()
                .email(user.getEmail())
                .password(user.getPassword()) //TODO: Hash it
                .firstname(user.getFirstname())
                .surname(user.getSurname())
                .points(0)
                .build();
        userRepository.save(newUser);

        roles.forEach(role -> {
            UserRole userRole = UserRole.builder().role(role).user(newUser).build();
            userRoleRepository.save(userRole);//TODO: Fix it
        });
        return "User " + user.getEmail() + " has been created";
    }

    @PostMapping("/user/changePassword")
    public Object changePassword() {
        return "TODO: changing password";
    }
}

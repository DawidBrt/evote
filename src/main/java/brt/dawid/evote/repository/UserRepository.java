package brt.dawid.evote.repository;

import brt.dawid.evote.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, String> {

    @Query("SELECT u FROM User u WHERE u.email = :email")
    Optional<User> findByEmail(@Param("email") String email);

    @Query("SELECT r.name FROM UserRole ur JOIN ur.user u JOIN ur.role r WHERE u.email = :email")
    List<String> findRolesByEmail(@Param("email") String email);
}

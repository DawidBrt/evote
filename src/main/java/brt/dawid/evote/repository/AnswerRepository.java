package brt.dawid.evote.repository;

import brt.dawid.evote.model.Answer;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnswerRepository extends CrudRepository<Answer, String> {

    @Query("SELECT a FROM Answer a JOIN a.question q WHERE q.id = :question_id")
    List<Answer> findAllAnswersToQuestion(@Param("question_id") String questionId);

    @Modifying(clearAutomatically = true)
    @Query("UPDATE Answer SET votes = " +
            "(SELECT old.votes + 1 FROM Answer old WHERE id = :id) " +
            "WHERE id = :id")
    void voteForAnswer(@Param("id") String id);
}

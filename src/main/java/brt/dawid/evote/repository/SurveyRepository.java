package brt.dawid.evote.repository;

import brt.dawid.evote.model.Survey;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SurveyRepository extends CrudRepository<Survey, String> {

    @Query("SELECT s FROM Survey s WHERE s.visible = true")
    List<Survey> findVisibleSurveys();

    @Query("SELECT s FROM Survey s JOIN s.user u WHERE u.email = :email")
    List<Survey> findSurveysByUser(@Param("email") String email);

    @Query("SELECT s FROM Survey s WHERE s.id = :id")
    Optional<Survey> findById(@Param("id") String id);

    @Query("SELECT s FROM Survey s WHERE s.name = :name")
    Optional<Survey> findByName(@Param("name") String name);
}

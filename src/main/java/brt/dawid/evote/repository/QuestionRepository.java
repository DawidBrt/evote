package brt.dawid.evote.repository;

import brt.dawid.evote.model.Question;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionRepository extends CrudRepository<Question, String> {

    @Query("SELECT q FROM Question q JOIN q.survey s WHERE s.id = :survey_id ORDER BY q.questionNumber ASC")
    List<Question> getAllQuestionsFromSurvey(@Param("survey_id") String surveyId);
}

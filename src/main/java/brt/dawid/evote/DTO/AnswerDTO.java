package brt.dawid.evote.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AnswerDTO {
    private String id;
    private String answer;
    private Integer votes;
}

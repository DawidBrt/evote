package brt.dawid.evote.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class SurveyDTO {
    private String name;
    private String note;
    private String password;
    private Boolean visible;
    private List<QuestionDTO> questions;
}

package brt.dawid.evote.DTO;

import lombok.Data;

@Data
public class CredentialsDTO {
    private String email;
    private String password;
}

package brt.dawid.evote.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class QuestionDTO {
    private String id;
    private String question;
    private Integer questionNumber;
    private List<AnswerDTO> answers;
}

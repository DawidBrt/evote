package brt.dawid.evote.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Set;

@Data
@AllArgsConstructor
public class UserDTO {
    private String email;
    private String password;
    private String confirmPassword;
    private String firstname;
    private String surname;
    private Set<String> roles;
}

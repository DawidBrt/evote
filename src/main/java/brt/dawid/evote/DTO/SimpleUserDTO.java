package brt.dawid.evote.DTO;


import lombok.RequiredArgsConstructor;
import lombok.Value;

@Value
@RequiredArgsConstructor(staticName = "of")
public class SimpleUserDTO {
    String email;
    String firstname;
    String surname;
    Integer points;
}

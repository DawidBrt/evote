package brt.dawid.evote.model;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    private String id;

    @Column(length = 256)
    private String email;

    @Column(length = 256)
    private String password;

    @Column(length = 256)
    private String firstname;

    @Column(length = 256)
    private String surname;

    private Integer points = 0;
}

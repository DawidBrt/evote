package brt.dawid.evote.model;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Survey {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(length = 256, unique = true)
    private String name;

    @Column(length = 1000)
    private String note;

    private Boolean passwordRequired = false;

    @Column(length = 256)
    private String password;

    private Boolean visible = true;
}

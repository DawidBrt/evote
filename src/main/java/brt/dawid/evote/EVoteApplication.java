package brt.dawid.evote;

import brt.dawid.evote.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.sql.DataSource;

@SpringBootApplication
@EnableJpaRepositories("brt.dawid.evote.repository")
@EntityScan("brt.dawid.evote.model")
public class EVoteApplication {

    @Autowired
    DataSource dataSource;

    @Autowired
    UserRepository userRepository;

    public static void main(String[] args) {
        final ConfigurableApplicationContext context = SpringApplication.run(EVoteApplication.class, args);
    }

}

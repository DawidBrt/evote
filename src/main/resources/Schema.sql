DROP TABLE IF EXISTS answer;
DROP TABLE IF EXISTS question;
DROP TABLE IF EXISTS survey;
DROP TABLE IF EXISTS user_role;
DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS role;

CREATE TABLE role (
    id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    name VARCHAR(20) NOT NULL
);

CREATE TABLE user (
    id VARCHAR(36) NOT NULL PRIMARY KEY,
    email VARCHAR(256) NOT NULL UNIQUE,
    password VARCHAR(256) NOT NULL,
    firstname VARCHAR(256),
    surname VARCHAR(256),
    points INT
);

CREATE TABLE user_role (
    id varchar(36) NOT NULL PRIMARY KEY,
    role_id INT NOT NULL,
    user_id VARCHAR(36) NOT NULL,
    FOREIGN KEY (role_id) REFERENCES role(id),
    FOREIGN KEY (user_id) REFERENCES user(id)
);

CREATE TABLE survey (
    id VARCHAR(36) NOT NULL PRIMARY KEY,
    user_id VARCHAR(36) NOT NULL,
    name VARCHAR(256) UNIQUE NOT NULL,
    note VARCHAR(1000),
    passwordRequired BOOLEAN NOT NULL DEFAULT FALSE,
    password VARCHAR(256),
    visible BOOLEAN NOT NULL DEFAULT TRUE,
    FOREIGN KEY (user_id) REFERENCES user(id)
);

CREATE TABLE question (
    id VARCHAR(36) NOT NULL PRIMARY KEY,
    survey_id VARCHAR(36) NOT NULL,
    question VARCHAR(256) NOT NULL,
    question_number INT NOT NULL,
    FOREIGN KEY (survey_id) REFERENCES survey(id),
    CONSTRAINT UQ_question UNIQUE (survey_id, question_number)
);

CREATE TABLE answer (
    id VARCHAR(36) NOT NULL PRIMARY KEY,
    question_id VARCHAR(36) NOT NULL,
    answer VARCHAR(256) NOT NULL,
    votes INTEGER DEFAULT 0,
    FOREIGN KEY (question_id) REFERENCES question(id)
);
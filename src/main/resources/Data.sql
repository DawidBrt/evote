INSERT INTO role(name)
VALUES ('ADMIN'),
       ('USER');

INSERT INTO user
VALUES ('0u', 'admin@evote.com', 'admin', 'Admin', 'Anonymous', 1000),
       ('1u', 'user@evote.com', 'user', 'User', 'Anonymous', 0);

INSERT INTO user_role
VALUES ('0ur', (SELECT r.id FROM role r WHERE r.name = 'ADMIN'),
        (SELECT u.id FROM user u WHERE u.email = 'admin@evote.com')),
       ('1ur', (SELECT r.id FROM role r WHERE r.name = 'USER'),
        (SELECT u.id FROM user u WHERE u.email = 'admin@evote.com')),
       ('2ur', (SELECT r.id FROM role r WHERE r.name = 'USER'),
        (SELECT u.id FROM user u WHERE u.email = 'user@evote.com'));

INSERT INTO survey
VALUES ('0s', (SELECT u.id FROM user u WHERE u.email = 'user@evote.com'),
        'First survey', 'Simple note', FALSE, null, TRUE),
       ('1s', (SELECT u.id FROM user u WHERE u.email = 'user@evote.com'),
        'Another survey', null, FALSE, null, FALSE);

INSERT INTO question
VALUES ('0q', '0s', 'How are you?', '1'),
       ('1q', '0s', 'Question2?', '2'),
       ('2q', '0s', 'Question3?', '3'),
       ('3q', '1s', 'Question4?', '1'),
       ('4q', '1s', 'Question5?', '2');

INSERT INTO answer
VALUES ('00a', '0q', 'Fine', 0),
       ('01a', '0q', 'Bad', 0),
       ('10a', '1q', 'Ans1', 10),
       ('11a', '1q', 'Ans2', 113),
       ('20a', '2q', 'Ans1', 0),
       ('21a', '2q', 'Ans2', 123124),
       ('22a', '2q', 'Ans3', 23),
       ('30a', '3q', 'Ans1', 0),
       ('31a', '3q', 'Ans2', 0),
       ('40a', '4q', 'Bad', 0),
       ('41a', '4q', 'Bad2', 0);